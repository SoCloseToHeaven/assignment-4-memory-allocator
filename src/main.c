//
// Created by Dmitry Lianguzov on 12/16/23.
//
#include "mem.h"
#include "mem_internals.h"
#include <unistd.h>
#define TEST_BYTES 5000
#define TEST_LEN 30
#define TEST_SIZE 0
#define TEST_MALLOC_SIZE 100



static void print_result(bool result, char* test_message) {
    if (result) {
        printf("Test: %s - PASSED", test_message);
        return;
    }
    printf("Test: %s - FAILED", test_message);
}


static void test_simple_allocate() {
    char* test_msg = "Basic allocation memory in heap #1";
    void* heap = heap_init(TEST_SIZE);
    void* block = _malloc(TEST_MALLOC_SIZE);

    if (heap == NULL || block == NULL) {
        print_result(false, test_msg);
        heap_term();
        return;
    }

    _free(block);
    heap_term();

    print_result(true, test_msg);
}

static void test_one_free() {
    char* test_msg = "Free one block in heap #2";
    heap_init(TEST_SIZE);
    void* mem = _malloc(TEST_SIZE);
    _free(mem);
    struct block_header* header = get_header(mem);
    if (!header->is_free) {
        print_result(false, test_msg);
        heap_term();
        return;
    }

    heap_term();
    print_result(true, test_msg);
}


static void test_free_blocks() {
    char* test_msg = "Free two blocks #3";
    heap_init(TEST_SIZE);
    void* mem = _malloc(TEST_SIZE);
    void* mem1 = _malloc(TEST_SIZE);
    _free(mem);
    _free(mem1);
    struct block_header* header = get_header(mem);
    struct block_header* header1 = get_header(mem1);
    if (!header->is_free || !header1->is_free) {
        print_result(false, test_msg);
        heap_term();
        return;
    }

    if (header->next != header1) {
        print_result(false, test_msg);
        heap_term();
        return;
    }

    heap_term();
    print_result(true, test_msg);
}

static void test_region() {
    char* test_msg = "Test merging regions #4";

    void* heap = heap_init(TEST_SIZE);

    void* block1 = _malloc(REGION_MIN_SIZE);
    struct block_header *h1 = get_header(block1);

    if (heap == NULL || block1 == NULL || h1->capacity.bytes != REGION_MIN_SIZE || h1->next != NULL) {
        print_result(false, test_msg);
        heap_term();
        return;
    }

    void* block2 = _malloc(TEST_MALLOC_SIZE);
    struct block_header *h2 = get_header(block1);

    if (block2 != h1->contents + REGION_MIN_SIZE + offsetof(struct block_header, contents) || h1->next != h2) {
        print_result(false, test_msg);
        heap_term();
        return;
    }

    _free(block2);
    _free(block1);
    munmap(heap, size_from_capacity(h1->capacity).bytes);

    print_result(true, test_msg);

}

static void test_new_region_in_another_place() {
    char* test_msg = "Creating new memory region in another place #5";
    void* odd_alloc = mmap(HEAP_START + REGION_MIN_SIZE + 1, TEST_LEN, PROT_READ | PROT_WRITE, MAP_PRIVATE
                                                                                        | 0x20, -1, 0 );
    heap_init(TEST_SIZE);
    uint8_t* mem = _malloc(TEST_BYTES);
    struct block_header* header = get_header(mem);

    if(!(header != odd_alloc && header->capacity.bytes == TEST_BYTES)) {
        print_result(false, test_msg);
        heap_term();
        return;
    }

    _free(mem);
    munmap(odd_alloc, TEST_LEN);
    heap_term();
    print_result(true, test_msg);
}

int main() {
    test_simple_allocate();
    test_one_free();
    test_free_blocks();
    test_region();
    test_new_region_in_another_place();
    return 0;
}

